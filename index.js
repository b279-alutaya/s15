
	let firstName = "John";
	let lastName = "Smith";
	console.log("First Name: " + firstName); 
	console.log("Last Name: " + lastName); 
	let currentAge1 = 30;
	console.log("Age: " + currentAge1);
	let myHobbiess = "Hobbies: ";
	console.log(myHobbiess)
	let myHobbies = ['Biking', 'Mountain Climbing', 'Swimming'];
	console.log(myHobbies);
	let workAddress = "Work Address: ";
	console.log(workAddress);

	let addressWork = {
	housenumber: '32',
  	street: 'Washington',
  	City: 'Lincoln',
  	state: 'Nebraska'
	};	

	console.log(addressWork);




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.

		Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
*/	

	let fullName = "Steve Rogers";
console.log("My full name is: " + fullName);

let currentAge = 40;
console.log("My current age is: " + currentAge);

let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
console.log("My Friends are: ");
console.log(friends);

let profile = {
    username: "captain_america",
    fullName: "Steve Rogers",
    age: 40,
    isActive: false,
};
console.log("My Full Profile: ");
console.log(profile);

let fullName2 = "Bucky Barnes";
console.log("My best friend is: " + fullName2);

let lastLocation = "Arctic Ocean";
lastLocation = "Arctic Ocean";
console.log("I was found frozen in: " + lastLocation);



	//Do not modify
	//For exporting to test.js
	//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
	try{
		module.exports = {
		    firstName: typeof firstName !== 'undefined' ? firstName : null,
		    lastName: typeof lastName !== 'undefined' ? lastName : null,
		    age: typeof age !== 'undefined' ? age : null,
		    hobbies: typeof hobbies !== 'undefined' ? hobbies : null,
		    workAddress: typeof workAddress !== 'undefined' ? workAddress : null,
		    fullName: typeof fullName !== 'undefined' ? fullName : null,
		    currentAge: typeof currentAge !== 'undefined' ? currentAge : null,
		    friends: typeof friends !== 'undefined' ? friends : null,
		    profile: typeof profile !== 'undefined' ? profile : null,
		    fullName2: typeof fullName2 !== 'undefined' ? fullName2 : null,
		    lastLocation: typeof lastLocation !== 'undefined' ? lastLocation : null
		}
	} catch(err){
	}